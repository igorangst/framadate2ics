# FramaDate to iCalendar

This is a simple script that takes a [FramaDate](https://framadate.org) poll and
generates an iCalendar file, containing all the available time slots as
tentative events. The resulting ics file can for example be imported in
Thunderbird. Each event has a description containing the original poll URL, all
the individual votes for the respective time slot (up to the time the script has
been called), as well as a summary of the number of yes/no/ifneedbe votes.

## Dependencies:

* python3
* [icalendar](https://pypi.org/project/icalendar/) 

## Usage:

The script takes three different arguments as input (recognized automatically):

1. A csv file that has been exported from the FramaDate poll site
2. A URL pointing to a FramaDate poll
3. Just the poll id, i.e. the last bizarre part of the FramaDate poll URL

The only additional option is `-o` or `--ouput` to supply an output file name
for the calendar.

