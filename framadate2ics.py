#! /usr/bin/env python3

import icalendar
import re
import sys
import urllib.request
import time

from html.parser import HTMLParser
from datetime import datetime
from optparse import OptionParser

# Parse time string and return (hour, minute) pair
def getTime(s):

    # International format HH:MM
    m = re.match('(\d+):(\d+)', s)
    if m:
        return (int(m.group(1)), int(m.group(2)))

    # French format HHhMM
    m = re.match('(\d+)h(\d+)', s)
    if m:
        return (int(m.group(1)), int(m.group(2)))

    # French format for full hours HHh
    m = re.match('(\d+)h', s)
    if m:
        return (int(m.group(1)), 0)

    # Numerical only format HHMM
    m = re.match('(\d{4})', s)
    if m:
        return (int(m.group(1)[0:1]), int(m.group(1)[2:3]))
    return None

def splitEntries(line):
    line = line.replace('""', '').strip('",\r')
    entries = line.split('","')
    return entries

def mkVotees(votes, n):
    votees = [[] for i in range(n)]
    for vote in votes:
        entries = splitEntries(vote)
        if len(entries) < 2:
            continue
        name = entries[0].strip('"')
        for i, answer in enumerate(entries[1:]):
            if answer in ["Yes", "Oui"]:
                votees[i].append(name)
    return votees

def listVotes(votes, i):
    descr = 'Votes:\n\n'
    summary = dict()
    for vote in votes:
        entries = splitEntries(vote)
        if len(entries) < 2:
            continue
        name = entries[0].strip('"')
        res = entries[i+1]
        descr += '%s: %s\n' % (name, res)
        if res in summary.keys():
            summary[res] += 1
        else:
            summary[res] = 1
    descr += '\nSummary:\n'
    sums = ['%d %s' % (v,k) for k,v in summary.items()]
    descr += ', '.join(sums)
    return descr

def main():
    usage = "%prog [options] <cvs file> | <poll id> | <poll url>"
    opt = OptionParser(usage)
    opt.add_option('-o', '--output', action='store', metavar='FILE', default=None,
                   dest='outputFile', help='Output file')
    opt.add_option('-f', '--filter', action='store_true', dest='filter',
                   default=False, help='Only copy slots with at least one vote')
    opt.add_option('-e', '--event', action='store', default='%p', metavar='FMT', 
                   dest='titleFormat', help='Set format string for event titles (you can use %p = poll title, %v = votees)')
    opt.add_option('-t', '--tentative', action='store_true', dest='tentative',
                   help='Set event status to tentative', default=False)
    (options, args) = opt.parse_args()

    # Check command line argument
    if len(args) < 1:
        print ('[ERROR] No csv file or poll URL given.')
        sys.exit(-1)
    inputArg = args[0]

    # Open CSV or retrieve it from the FramaDate website
    if inputArg.endswith('.csv'):

        # It's a local CSV file
        with open(inputArg, 'r') as f:
            lines = f.read().split('\n')
        title = inputArg[:-4]
        url = title
    else:
        if inputArg.startswith('https://') or inputArg.startswith('http://'):

            # It's a URL, get poll id from string
            pos = inputArg.rfind('/')
            pollid = inputArg[pos+1:]
        else:

            # It's probably a poll id
            # FIXME: No further check, could be anything else
            pollid = inputArg

        url = 'https://framadate.org/exportcsv.php?poll=' + pollid
        cvs = urllib.request.urlopen(url).read().decode('utf-8')
        lines = cvs.split('\n')
        url = 'https://framadate.org/' + pollid
        html = urllib.request.urlopen(url).read().decode('utf-8').split('\n')
        title = None
        for line in html:
            m = re.match('\s*<title>(.+)</title>', line)
            if m:
                title = m.group(1)
                break
        if title is None:
            print ('AAAAAAAHHHH!')
            sys.exit(-2)
        htmlParser = HTMLParser()
        title = htmlParser.unescape(title)
        if title.startswith('Sondage - '):
            title = title[10:]
        elif title.startswith('Poll - '):
            title = title[7:]
        if title.endswith(' - Framadate'):
            title = title[:-12]

    print ("Creating iCalendar for Poll '%s'" % title)

    # Apply format string to title
    summaryFormat = options.titleFormat.replace('%p', title)

    # Create calendar object to be populated
    cal = icalendar.Calendar()

    # Extract first two lines containing dates and times
    dates = lines[0].split(',')[1:]
    times = lines[1].split(',')[1:]
    dates = [s.strip('"') for s in dates]
    times = [s.strip('"') for s in times]
    votes = lines[2:]
    votees = mkVotees(votes, len(dates))

    # Get current time for time stamps
    now = time.localtime()

    # Create ical events
    for i in range(len(dates)):
        d = dates[i]
        t = times[i]
        if d.strip() == '':
            continue

        dstart = ""
        m = re.match('(\d\d\d\d)-(\d\d)-(\d\d)', d)
        if m:
            year = int(m.group(1))
            month = int(m.group(2))
            day = int(m.group(3))
        else:
            m = re.match ('(\d\d)/(\d\d)/(\d\d\d\d)', d)
            if m:
                year = int(m.group(3))
                month = int(m.group(2))
                day = int(m.group(1))
            else:
                print ("[WARNING] Unknown date format '%s'. Skipping entry." % d)
                continue
        m = re.match('(.+)\s*-+\s*(.+)', t)
        if m:
            start_hour, start_minute = getTime(m.group(1))
            end_hour, end_minute = getTime(m.group(2))
        else:
            print ("[WARNING] Unknown time format '%s'. Skipping entry." % t)
            continue

        if options.filter and not votees[i]:
            print ('Drop event with zero votes...')
            continue

        descr = 'Poll: ' + url + '\n\n' + listVotes(votes, i)
        voteeString = ', '.join(votees[i])
        summary = summaryFormat.replace('%v', voteeString)
        print ("Create event '{}'". format(summary))

        event = icalendar.Event()
        event.add('summary', summary)
        event.add('dtstart', datetime(year, month, day, start_hour, start_minute))
        event.add('dtend', datetime(year, month, day, end_hour, end_minute))
        event.add('dtstamp', datetime(now[0], now[1], now[2], now[3], now[4]))
        event.add('description', descr)
        if options.tentative:
            event.add('status', 'TENTATIVE')
        else:
            event.add('status', 'CONFIRMED')
        cal.add_component(event)

        # TODO
        # - ORGANIZER:MAILTO entry with mail from framadate page
        # - DURATION entry default 1h if no end is given


    if options.outputFile is None:
        options.outputFile = title + '.ics'
    with open(options.outputFile, 'w') as f:
        print ("Writing iCalendar to file '%s'" % options.outputFile)
        f.write(cal.to_ical().decode('utf-8'))

if __name__ == "__main__":
    main()
